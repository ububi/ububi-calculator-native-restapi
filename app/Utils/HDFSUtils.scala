/** ---------------------------
  * PROJECT: UBUBI
  * Auth:
  * Francois Saab
  * Mail: saab.francois@gmail.com, francois.saab.1@ens.etsmtl.ca
  * Date: 1/1/2017
  *
  * Copyright © 2017 Francois Saab
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  * -------------------------- */
package Utils

import java.nio.file.{Paths, Files}

import Models.Connection
import play.api.libs.ws.{WSClient, WSResponse, WSRequest}

import scala.concurrent.{Await, Future}
import scala.util.Success

import scala.concurrent.duration._
/**
  * Created by francoissaab on 1/11/18.
  */
class HDFSUtils(ws: WSClient) {

  def getHDFSJarPath(sparkconn:Connection)={

    var sb:StringBuilder= new StringBuilder()

    sb.append("http://").append(sparkconn.IP).append(":").append(sparkconn.PortNumber).append("/batches")

    sb.toString()
  }



}

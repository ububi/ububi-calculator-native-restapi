/** ---------------------------
  * PROJECT: UBUBI
  * Auth:
  * Francois Saab
  * Mail: saab.francois@gmail.com, francois.saab.1@ens.etsmtl.ca
  * Date: 1/1/2017
  *
  * Copyright © 2017 Francois Saab
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  * -------------------------- */
package Utils

import Models.funcs
import play.api.libs.ws.WSClient
import play.api.Configuration

/**
  * Created by francoissaab on 1/11/18.
  */
object ScalaRunner{

  def closestUpperNumber(n: Int, m: Int): Int = { // find the quotient
    val q = n / m
    // 1st possible closest number
    val n1 = m * q
    // 2nd possible closest number
    val n2 = if ((n * m) > 0) m * (q + 1)
    else m * (q - 1)
    // if true, then n1 is the required closest number
    if (Math.abs(n - n1) < Math.abs(n - n2)) return n1
    // else n2 is the required closest number
    n2
  }

  def runSingle(ws: WSClient,config:Configuration,
                ProcessId:Long, quantity:Double, DBTemplateId:Int, ProjectId:Long,
                LCIA:Boolean, LCIAMethod:Int,montecarlo:Boolean,Montecarlo_Iterations:Int,
                version:Int, LASolveMethod:String,systemId:String,calciD:String)
                :MessagingLocal.Message={

    val destFD=funcs.localDestFilePath(ProjectId,version)

    val parallelism= Runtime.getRuntime().availableProcessors()*3
    val sensitivity= config.getString("SENSITIVITY").get
    val minimum_iterations=config.getString("MINIMUM_ITERATIONS").get.toInt

    val params_to_calculator=Array(ProcessId.toString,
      quantity.toString,
      LASolveMethod,
      systemId,
      calciD,
      LCIAMethod.toString,
      LCIA.toString,
      montecarlo.toString,
      if(Montecarlo_Iterations<minimum_iterations){
        closestUpperNumber(minimum_iterations,parallelism).toString}else{
        closestUpperNumber(Montecarlo_Iterations, parallelism ).toString
      },
      ProjectId.toString,
      version.toString,
      parallelism.toString,sensitivity
    )


    //call calculator
    org.ets.lca.LCASingle.main(params_to_calculator)
    //get Message object representign the calculation
    val message_from_calculator=org.ets.lca.LCASingle.getMessage()
     //
    new MessagingLocal.Message(message_from_calculator.Code,message_from_calculator.Message)

  }

}

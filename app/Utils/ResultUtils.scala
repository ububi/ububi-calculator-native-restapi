/** ---------------------------
  * PROJECT: UBUBI
  * Auth:
  * Francois Saab
  * Mail: saab.francois@gmail.com, francois.saab.1@ens.etsmtl.ca
  * Date: 1/1/2017
  *
  * Copyright © 2017 Francois Saab
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  * -------------------------- */
package Utils

import java.nio.file.{Paths, Files}

import Models.Connection
import play.api.libs.json.{JsObject, Json}

/**
  * Created by francoissaab on 1/12/18.
  */
class ResultUtils {

  def getResults(state:String,
                  calciD:String,
                  serverconn:Connection,
                  LCIAMethod:Int,
                  montecarlo:Boolean,
                  version:Int,
                  message:MessagingLocal.Message
                 ):JsObject={

    if(state=="\"success\"") {


     /* putHDFS("data/calculations/" + calciD +"/lci"+"_"+calciD+".txt",
        "lci"  + "_" + calciD + ".txt")*/

      val lciContributionUrl = "http://" + serverconn.IP + ":" + serverconn.PortNumber + "/" + "getFile/" +
        "lci"  + "_" + calciD + ".txt"

      /*putHDFS("data/calculations/" + calciD ++"/lcia_"+"_"+calciD+"_"+LCIAMethod+".txt",
        "lcia"  + "_" + calciD + "_" + LCIAMethod + ".txt")*/

      val lciaUrl = "http://" + serverconn.IP + ":" + serverconn.PortNumber + "/" + "getFile/" +
        "lcia"  + "_" + calciD  + ".txt"

      /*putHDFS("data/calculations/" + calciD +"/lcia_tree_"+"_"+calciD+".txt",
        "lcia_graph"  + "_" + calciD + "_" + LCIAMethod + ".txt")*/


      val graphLciaUrl = "http://" + serverconn.IP + ":" + serverconn.PortNumber + "/" + "getFile/" +
        "lcia-tree"  + "_" + calciD  + ".txt"

      /*putHDFS("data/calculations/" + calciD +"/lci_tree_"+ systemId+"_"+calciD+".txt",
                  "lci_graph_" + systemId + "_" + calciD + "_" + LCIAMethod + ".txt")

       val graphLciUrl = "http://" + serverconn.IP + ":" + serverconn.PortNumber + "/" + "getFile/" +
                  "lci_graph_" + systemId + "_" + calciD + "_" + LCIAMethod + ".txt"
      */

      /*val unc_lciUrl = "http://" + serverconn.IP + ":" + serverconn.PortNumber + "/" + "getFile/" +
        "uncertainty_lci_" + systemId + "_" + calciD  + ".txt"*/

      /*if(Files.exists(Paths.get("data/calculations/" + calciD +"/uncertainty_lcia_"  + "_" + calciD+ "_" + LCIAMethod + ".txt"))) {
        putHDFS("data/calculations/" + calciD + "/uncertainty_lcia_"  + "_" + calciD + "_" + LCIAMethod + ".txt",
          "uncertainty_lcia"  + "_" + calciD + "_" + LCIAMethod + ".txt")
      }*/

      val unc_lciaUrl = "http://" + serverconn.IP + ":" + serverconn.PortNumber + "/" + "getFile/" +
        "uncertainty-lcia"  + "_" + calciD + ".txt"

      /*if(Files.exists(Paths.get("data/calculations/" + calciD +"/SENS_lcia_"  + "_" + calciD + ".txt"))) {

        putHDFS("data/calculations/" + calciD + "/SENS_lcia_"  + "_" + calciD + ".txt",
          "SENS_lcia"  + "_" + calciD + ".txt")

      }*/

      val SENS_lciaUrl = "http://" + serverconn.IP + ":" + serverconn.PortNumber + "/" + "getFile/" +
        "SENS-lcia" + "_" + calciD + ".txt"

      if(montecarlo){ Json.obj(
        "state" -> state,
        "version"-> version,
        "file_lci" -> lciContributionUrl,
        "file_lcia" -> lciaUrl,
        "file_graph_lcia" -> graphLciaUrl,
        /*"file_graph_lci" -> graphLciUrl,*/
        "file_sens_lcia" -> SENS_lciaUrl,
        "file_lcia_unc" -> unc_lciaUrl,
        "code" -> message.Code,
        "message"->message.Message,
        "calculationid"-> calciD
      )
      }else{
        Json.obj(
          "state" -> state,
          "version"-> version,
          "file_lci" -> lciContributionUrl,
          "file_lcia" -> lciaUrl,
          "file_graph_lcia" -> graphLciaUrl,
          "code" -> message.Code,
          "message"->message.Message,
          "calculationid"-> calciD

        )

      }
    }
    else{

      println("failed.........")

     Json.obj(
        "state" -> state,
        "version"-> version,
        "code" -> message.Code,
        "message"->message.Message,
        "calculationid"-> calciD
      )


    }
  }

}

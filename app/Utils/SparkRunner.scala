/** ---------------------------
  * PROJECT: UBUBI
  * Auth:
  * Francois Saab
  * Mail: saab.francois@gmail.com, francois.saab.1@ens.etsmtl.ca
  * Date: 1/1/2017
  *
  * Copyright © 2017 Francois Saab
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  * -------------------------- */

package Utils


import Messaging.Message
import Models.{Connection, funcs}
import play.api.libs.json.{JsValue, Json, JsObject}
import play.api.libs.ws.{WSClient, WSResponse, WSRequest}

import scala.concurrent.{Await, Future}
import scala.util.Success
import scala.language.postfixOps
import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global

//import org.ets.lca.Single

/**
  * Created by francoissaab on 1/13/17.
  */
object SparkRunner {

  def run(ws: WSClient,
          ProcessId:Int, quantity:Double, DBTemplateId:Int, ProjectId:Long, LCIA:Boolean, LCIAMethod:Int,montecarlo:Boolean,Montecarlo_Iterations:Int,
          version:Int,
          LASolveMethod:String,systemId:String,calciD:String):String={


    val spconn=new Connection( "spark.hadoop" ,"8998")

    val jarURL= new HDFSUtils(ws).getHDFSJarPath(spconn)


    val request1: WSRequest = ws.url(jarURL)

    val jsonRequest1: WSRequest = request1.withHeaders("Accept" -> "application/json")

    val destFD=funcs.hdfsDestFilePath(ProjectId,version)

    val data = Json.obj(
      "file" -> "hdfs://namenode.hadoop/user/root/jars/ubuubi-lca-calculator_2.11-1.0.jar",
      /*"file" -> "file:///usr/local/spark-lca/ubuubi-lca-calculator_2.11-1.0.jar",*/
      "className" -> "org.ets.lca.LCASpark",
      "numExecutors" -> 1, "executorCores" -> 6, "executorMemory" -> "8g", "driverMemory" -> "40g",
      "driverCores" -> 25,
      "args" -> Json.toJson( Array(ProcessId.toString, quantity.toString, LASolveMethod, systemId, calciD,
        LCIAMethod.toString,
        LCIA.toString,
        montecarlo.toString,
        Montecarlo_Iterations.toString,
        destFD+"exchanges.spark",
        destFD+"processes.spark",
        destFD+"flows.spark",
        destFD+"units.spark",
        destFD+"impact_methods.spark",
        destFD+ "impact_categories.spark",
        destFD+"impact_factors.spark",
        destFD+"parameters.spark"
      ))
    )

    //{"id":5,"state":"starting","appId":null,"appInfo":{"driverLogUrl":null,"sparkUiUrl":null},"log":[]}
    val futureResponse1: Future[WSResponse] = jsonRequest1.post(data)

    var f1: Future[JsValue] = futureResponse1.map(resp=> (resp.json ).as[JsValue])

    Await.result( f1,20 seconds)

    println(f1.value)

    val id= f1.value
      .get
      .asInstanceOf[Success[JsObject]].value
      .asInstanceOf[JsObject].value("id")

    val statusUrl="http://"+spconn.IP+":"+spconn.PortNumber+"/batches/"+ id
    val request2: WSRequest = ws.url(statusUrl)
    val jsonRequest2: WSRequest = request2.withHeaders("Accept" -> "application/json")
    var futureResponse2: Future[WSResponse] = jsonRequest2.get()
    var f2: Future[JsValue]= futureResponse2.map(resp=> (resp.json ).as[JsValue])
    Await.result( f2,5 seconds)

    var state:String= f2.value
      .get.asInstanceOf[Success[JsObject]]
      .value.asInstanceOf[JsObject].value("state").toString()

    /*Session State

      value	      description
      not_started	Session has not been started
      starting	  Session is starting
      idle	      Session is waiting for input
      busy	      Session is executing a statement
      error	      Session errored out
      dead	      Session has exited*/

    while(!(state =="\"success\"" || state =="\"dead\"")){

      val futureResponse3 = jsonRequest2.get()
      val f3= futureResponse3.map(resp=> (resp.json ).as[JsValue])
      //pending: replace of await
      Await.result( f3,5 seconds)
      state= f3.value
        .get.asInstanceOf[Success[JsObject]]
        .value.asInstanceOf[JsObject].value("state").toString()
    }

    state

  }



}
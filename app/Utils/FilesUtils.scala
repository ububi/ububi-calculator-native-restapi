/** ---------------------------
  * PROJECT: UBUBI
  * Auth:
  * Francois Saab
  * Mail: saab.francois@gmail.com, francois.saab.1@ens.etsmtl.ca
  * Date: 1/1/2017
  *
  * Copyright © 2017 Francois Saab
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  * -------------------------- */
package Utils

import java.io.{BufferedWriter, File, FileWriter}
import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

import play.api.libs.ws.WSClient

import scala.collection.JavaConversions._
import scala.io.Codec
import scala.language.postfixOps
/**
  * Created by francoissaab on 12/20/16.
  */

class FilesUtils(ws: WSClient) {

  /*def deltaLines(deltas:String):List[String]={

    Files.readAllLines(Paths.get(deltas))

  }*/

  def mergeDeltas(ProjectId:Long,originalPath:String,originalFN:String, deltas:String, version:Int):Unit={

    val fieldSep=29.toChar.toString

    val itemDeltas=Files.readAllLines(Paths.get(deltas),StandardCharsets.ISO_8859_1)


    if(!itemDeltas.isEmpty) {

      //Pending: optimise
      val itemLines=Files.readAllLines( Paths.get( originalPath+originalFN),StandardCharsets.ISO_8859_1)

      var addDeltas = scala.collection.mutable.HashMap.empty[Long, String]

      //var updateDeltas = scala.collection.mutable.HashMap.empty[Long, String]

      var removeDeltas = scala.collection.mutable.HashMap.empty[Long, String]

      for (line <- itemDeltas) {

        val arr: Array[String] = line.split(fieldSep)

        val op = arr(0)

        val id = arr(1)

        val record = arr.drop(1).mkString(fieldSep)

        op match {

          case "insert" => addDeltas += (id.toLong -> record)

          case "update" => addDeltas += (id.toLong -> record)

          case "delete" => removeDeltas += (id.toLong -> record)

        }

      }

      // pending optimise
      val itemLinesFiltered = itemLines.filter(x =>
        !addDeltas.keys.contains(x.split(fieldSep)(0).toLong)
          && !removeDeltas.keys.contains(x.split(fieldSep)(0).toLong))


      addDeltas.values.foreach(itemLinesFiltered.add(_))


      Files.write(Paths.get("data/projects/"+ProjectId+"/DBVersions/" + version + "/" + originalFN), itemLinesFiltered)

    }else{

      copyAsIs(ProjectId,originalPath ,originalFN,version)
    }

  }

  def copyAsIs(ProjectId:Long,originalPath:String,originalFN:String, version:Int):Unit={

    val path= Paths.get("data/projects/"+ProjectId+"/DBVersions/" + version + "/" + originalFN)

    if(!Files.exists(path)) {

      Files.copy(Paths.get( originalPath+originalFN),path)

    }


  }


  def writeTextToFile(text:String,path:String)={


    try {
      val file = new File(path)
      file.createNewFile()
      val bw = new BufferedWriter(new FileWriter(file))
      bw.write(text)
      bw.close()
    } catch {
      case e: Exception => {
        println(e.getMessage+"--> "+path)
      }
    }

  }


  def getListOfSubDirectories(directoryName: String): Array[String] = {


    val subDirs:Array[String]=

      try{
        (new File(directoryName)).listFiles.filter(_.isDirectory).map(_.getName)
      }catch{

        case e: Exception => {

          val empty:Array[String]=Array()

          empty

        }


      }

    subDirs
  }

  def getListOfFiles(directoryName: String): Array[String] = {


    val subDirs:Array[String]=

      try{
        (new File(directoryName)).listFiles.filter(_.isFile).map(_.getName)
      }catch{

        case e: Exception => {

          val empty:Array[String]=Array()

          empty

        }


      }

    subDirs
  }

  def readTextFromFile(path:String)={

    import scala.io.Source

    Source.fromFile(path)(Codec.ISO8859).getLines.toVector

  }
}

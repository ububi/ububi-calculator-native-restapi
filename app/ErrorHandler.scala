import java.io.FileWriter
import java.util.Calendar
import javax.inject.{Inject, Provider}


import MessagingLocal.CustomException
import play.api._
import play.api.http.DefaultHttpErrorHandler
import play.api.http.Status._
import play.api.libs.json.Json
import play.api.mvc.Results._
import play.api.mvc._
import play.api.routing.Router
import play.core.SourceMapper

import scala.concurrent._

/**
  * Provides a stripped down error handler that does not use HTML in error pages, and
  * prints out debugging output.
  *
  * https://www.playframework.com/documentation/2.5.x/ScalaErrorHandling
  */
class ErrorHandler(environment: Environment,
                   configuration: Configuration,
                   sourceMapper: Option[SourceMapper] = None,
                   optionRouter: => Option[Router] = None)
    extends DefaultHttpErrorHandler(environment,
                                    configuration,
                                    sourceMapper,
                                    optionRouter) {

  private val logger =
    org.slf4j.LoggerFactory.getLogger("application.ErrorHandler")

  // This maps through Guice so that the above constructor can call methods.
  @Inject
  def this(environment: Environment,
           configuration: Configuration,
           sourceMapper: OptionalSourceMapper,
           router: Provider[Router]) = {
    this(environment,
         configuration,
         sourceMapper.sourceMapper,
         Some(router.get))
  }

  override def onClientError(request: RequestHeader,
                             statusCode: Int,
                             message: String): Future[Result] = {
    logger.debug(
      s"onClientError: statusCode = $statusCode, uri = ${request.uri}, message = $message")

    Future.successful {
      val result = statusCode match {
        case BAD_REQUEST =>
          Results.BadRequest(message)
        case FORBIDDEN =>
          Results.Forbidden(message)
        case NOT_FOUND =>
          Results.NotFound(message)
        case clientError if statusCode >= 400 && statusCode < 500 =>
          Results.Status(statusCode)
        case nonClientError =>
          val msg =
            s"onClientError invoked with non client error status code $statusCode: $message"
          throw new IllegalArgumentException(msg)
      }
      result
    }
  }

  override protected def onDevServerError(
      request: RequestHeader,
      exception: UsefulException): Future[Result] = {


    println(s"ServerError on dev:  uri = ${request.uri}, message = ${exception.getStackTrace}")



    val result = Json.obj("state" -> "Dead",
      "version"-> 1,
      "message"->exception.getStackTrace.mkString(System.lineSeparator),
      "time"-> Calendar.getInstance.getTime.toString)


   /* val custexc=exception.asInstanceOf[CustomException]


    val result =Json.obj(
          "state" -> "Failed",
          "version"-> -1,
          "code" -> custexc.message.Code,
          "message"->custexc.message.Message,
          "calculationid"-> ""
        )*/

    /*val fw = new FileWriter("logs/error.log", true)
    try {
      fw.write(

        "---------------------------------------------------------------------"+
          System.lineSeparator+
        result.toString()+
        System.lineSeparator+
        "---------------------------------------------------------------------"+
          System.lineSeparator+ System.lineSeparator+System.lineSeparator
      )
    }
    finally fw.close()*/

   Future.successful(Ok(result))

   /*

    Future.successful(
      InternalServerError(Json.obj("TEST exception" -> exception.toString)))
*/
  }

  override protected def onProdServerError(
      request: RequestHeader,
      exception: UsefulException): Future[Result] = {


    logger.error(
      s"ServerError by Francois on production:  uri = ${request.uri}, message = ${1}",exception.getStackTrace)


    println(s"ServerError by Francois on production:  uri = ${request.uri}, message = ${exception.getStackTrace}")

    Future.successful(InternalServerError)
  }
}

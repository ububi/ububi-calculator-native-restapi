/** ---------------------------
  * PROJECT: UBUBI
  * Auth:
  * Francois Saab
  * Mail: saab.francois@gmail.com, francois.saab.1@ens.etsmtl.ca
  * Date: 1/1/2017
  *
  * Copyright © 2017 Francois Saab
  *
  * Licensed under the Apache License, Version 2.0 (the "License");
  * you may not use this file except in compliance with the License.
  * You may obtain a copy of the License at
  *
  * http://www.apache.org/licenses/LICENSE-2.0
  *
  * Unless required by applicable law or agreed to in writing, software
  * distributed under the License is distributed on an "AS IS" BASIS,
  * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  * See the License for the specific language governing permissions and
  * limitations under the License.
  *
  * -------------------------- */


package controllers


import java.nio.file.{Files, Paths}
import java.util.{ Random}

import javax.inject.Inject
import MessagingLocal.{Message, MessagingUtils}
import Models.{Connection, funcs}
import Utils._

import org.ets.lca.DAL.{LCADBSet, lcadb}

import scala.concurrent.{Await, Future}
import scala.concurrent.duration._
import play.api.mvc.{Action, Controller}
import play.api.libs.ws._

import scala.concurrent.ExecutionContext.Implicits.global
import play.api.libs.json._

import scala.language.postfixOps
import scala.util.Success
import play.api.Configuration
/**
  * Controller to get launch calculator instances.
  */
class HomeController @Inject() (ws: WSClient,config: Configuration) extends Controller {

  val fileUtils=new FilesUtils(ws)

  val serverconn=new Connection( config.getString("IP_LOCAL").get , config.getString("PORT_LOCAL").get )

  def getLogs ()= Action{

  val lines = scala.io.Source.fromFile("logs/error.log").mkString

  Ok(lines)

  }

  def getFile (file:String)= Action{

  val start=file.indexOf("_")+1
  val end=file.indexOf(".")

  val calcid=file.substring(start,end)
  val name=file.substring(0,file.indexOf("_"))


  val str= (name match {

    case "SENS-lcia" =>{

      new FilesUtils(ws).getListOfFiles("data/calculations/" + calcid+"/SensitivityPerImpactCat/").flatMap(x=>{

        new FilesUtils(ws).readTextFromFile("data/calculations/" + calcid+"/SensitivityPerImpactCat/"+ x)
      }).mkString(sys.props("line.separator"))

    }

    case "lcia-tree" =>{

      new FilesUtils(ws).getListOfFiles("data/calculations/" + calcid+"/lcia-tree/").flatMap(x=>{

        new FilesUtils(ws).readTextFromFile("data/calculations/" + calcid+"/lcia-tree/"+ x)
      }).mkString(sys.props("line.separator"))

    }

    case _ => new FilesUtils(ws).readTextFromFile("data/calculations/" + calcid+"/"+file)
      .mkString(sys.props("line.separator"))
  }
   )

  Ok(str)

  }


  /** Get DB templates
    *
    * @return JSON result array of DBTemplate ids {[id1,id2,id3,...]}
    */
  def DBTemplates() = Action{

  val DBs= fileUtils.getListOfSubDirectories("data/DBTemplates/")

  val DBsDescr= (DBs zip ( DBs.map(  _ match {

    case "1" => "ecoinvent3.3 - Active"

    case "2" => "ecoinvent3.2+wall quebec + no uncertainty - not active"

    case "6" => "ecoinvent3.2 - Active"

    case _ => "Not yet defined - not active"


  }))).map(x=> Json.toJson(Array(x._1,x._2)))

  val result = Json.obj(
    "DBTemplates" -> Json.toJson( DBsDescr)
  )

  Ok( result.toString())

  }
  /** Get projects
    *
    * @return JSON result array of DBTemplate ids {[id1,id2,id3,...]}
    */
  def projects() = Action{

  val projs= fileUtils.getListOfSubDirectories("data/projects/").map(Json.toJson(_))

  val result = Json.obj(

    "projects" -> Json.toJson( projs)
  )

  Ok( result.toString())

  }
  /** Get project versions of a given project id as POST
    *
    * @param  Project Id
    * @return JSON result array of project ids {[id1,id2,id3,...]}
    */
  def projectVersions(ProjectId:Long) = Action{

  val versions= fileUtils.getListOfSubDirectories(s"data/projects/$ProjectId/DBVersions/").map(Json.toJson(_))

  val result = Json.obj(
    "project"-> Json.toJson(ProjectId),

    "versions" -> Json.toJson( versions)
  )

  Ok( result.toString())

  }

  /** Calculate LCA based on given parameters via POST
    *
    * @param  ProcessId
    * @param  quantity
    * @param  DBTemplateId
    * @param  ProjectId
    * @param  LCIA
    * @param  LCIAMethod
    * @param  montecarlo
    * @param  Montecarlo_Iterations
    * @param  version
    * @param  prefix
    * @param   exchanges_deltas
    * @param   flows_deltas
    * @param   processes_deltas
    * @param   units_deltas
    * @param   impact_methods_deltas
    * @param   impact_categories_deltas
    * @param   impact_factors_deltas
    * @param   parameters_deltas
    * @return JSON result of the calculation

    */

  def calculate() = Action(parse.anyContent) {

  request =>

    val mpform=request.body.asMultipartFormData

    var ProcessId:Long= 0
    var quantity:Double=  0
    var DBTemplateId:Int=  0
    var ProjectId:Long= 0
    var LCIA:Boolean= false
    var LCIAMethod:Int= -1
    var montecarlo:Boolean=false
    var Montecarlo_Iterations:Int=0
    var version:Int= 0
    var prefix:String=""


    val defaultLASolveMethod:String= "solve-local-sparse-umfpack"

    if (mpform!=None) {

      val mpformdata=request.body.asMultipartFormData.get

      // get posted deltas as files
      val deltas = Array(
        ("ExchangesDiff.txt", mpformdata.file("exchanges_deltas")),
        ("FlowsDiff.txt", mpformdata.file("flows_deltas")),
        ("ProcessesDiff.txt", mpformdata.file("processes_deltas")),
        ("UnitsDiff.txt", mpformdata.file("units_deltas")),
        ("ImpactMethodsDiff.txt", mpformdata.file("impact_methods_deltas")),
        ("ImpactCategoriesDiff.txt", mpformdata.file("impact_categories_deltas")),
        ("ImpactFactorsDiff.txt", mpformdata.file("impact_factors_deltas")),
        ("ParametersDiff.txt", mpformdata.file("parameters_deltas"))
      )

      try {
        version = mpformdata.dataParts("version").head.toInt
        ProcessId = mpformdata.dataParts("ProcessId").head.toLong
        quantity = mpformdata.dataParts("quantity").head.toDouble
        ProjectId = mpformdata.dataParts("ProjectId").head.toLong
        LCIA = mpformdata.dataParts("LCIA").head.toBoolean
        LCIAMethod = mpformdata.dataParts("LCIAMethod").head.toInt
        montecarlo = mpformdata.dataParts("MonteCarlo").head.toBoolean
        Montecarlo_Iterations = mpformdata.dataParts("Montecarlo_Iterations").head.toInt
        prefix = mpformdata.dataParts("prefix").head.toString
      }catch{

        case (e:java.lang.NumberFormatException) =>(new MessagingUtils()).throwMessageAsException(new Message(5,"Invalid Form Inputs"))

        case (e:java.lang.IllegalArgumentException) => (new MessagingUtils()).throwMessageAsException(new Message(5,"Invalid Form Inputs"))

      }

      if(mpformdata.dataParts.contains("DBTemplateId")) {

        val origPath = version match {

          case 1 => {

            DBTemplateId = mpformdata.dataParts("DBTemplateId").head.toInt

            "data/DBTemplates/" + DBTemplateId + "/"
          }

          case _ => "data/projects/" + ProjectId + "/DBVersions/" + (version - 1) + "/"

        }

        Files.createDirectories(Paths.get("data/projects/" + ProjectId + "/DBVersions/" + version + "/"))
        Files.createDirectories(Paths.get("data/projects/" + ProjectId + "/DBVersions/" + version + "/deltas"))

        val destFD = funcs.destFilePath(ProjectId, version)
        import java.nio.file.StandardCopyOption.REPLACE_EXISTING
        deltas.foreach(d => {

          if (d._2 != None) // merge if file is uploaded. copy if uploaded file is empty
          {

            Files.copy(Paths.get(d._2.get.ref.file.toPath.toString),
              Paths.get("data/projects/" + ProjectId + "/DBVersions/" + version + "/deltas/"+d._1)
              ,REPLACE_EXISTING )

          }

        })

      }
    }

    val systemId:String= prefix+"_"+ProjectId+"_"+"v"+version+"_"+ProcessId.toString//+"_"+ (new Random()).nextInt()

    val calciD:String=  prefix+"_"+ProjectId+"_"+"v"+version+"_"+ProcessId.toString+"_"+ (new Random()).nextInt()

    val message=ScalaRunner.runSingle(ws,config,ProcessId,quantity,DBTemplateId,ProjectId,
      LCIA,LCIAMethod,montecarlo,Montecarlo_Iterations,version,defaultLASolveMethod,systemId,calciD)

    val state=if(message.Code==1) "\"success\"" else "\"failed\""

    val result = new ResultUtils().getResults(state,
      calciD,
      serverconn,
      LCIAMethod,
      montecarlo,
      version,
      message)

    Ok( result.toString())
  }
  /** Reset DB given its project id
    *
    * @param  ProjectId
    * @return JSON object descriving the result of the operation
    */
  def reset() = Action(parse.tolerantFormUrlEncoded) {

      request =>

        var ProjectId:Long= 0

          try {

            ProjectId=request.body.get("ProjectId").get.head.toLong
          } catch {

            case (e: java.lang.NumberFormatException) => (new MessagingUtils()).throwMessageAsException(new Message(5, "Invalid Form Inputs"))

            case (e: java.lang.IllegalArgumentException) => (new MessagingUtils()).throwMessageAsException(new Message(5, "Invalid Form Inputs"))

          }

        var result =Json.obj(
          "state" -> "Success",
          "version"-> -1,
          "code" -> 1,
          "message"->"",
          "calculationid"-> "-1"
        )

        if(ProjectId>0) {
          LCADBSet.lcadbs(ProjectId).resetDB(ProjectId,true)
        }else{
          result = Json.obj(
            "state" -> "Failed.",
            "version"-> -1,
            "code" -> 1,
            "message"->"",
            "calculationid"-> "-1"
          )
        }

        Ok( result.toString())
    }

}

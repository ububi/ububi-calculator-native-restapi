import sbt.Keys._
import com.amazonaws.auth.{AWSCredentialsProviderChain, DefaultAWSCredentialsProviderChain}
import com.amazonaws.auth.profile.ProfileCredentialsProvider

s3CredentialsProvider := { (bucket: String) =>
  new AWSCredentialsProviderChain(
    new ProfileCredentialsProvider("default"),
    DefaultAWSCredentialsProviderChain.getInstance()
  )
}

resolvers += "Releases resolver" at "s3://repo.protogest.net/release"
resolvers += "Snapshots resolver" at "s3://repo.protogest.net/snapshot"

name := "play-rest-api"
organization := "org.ets.lca"
scalaVersion := "2.11.8"

libraryDependencies += "com.netaporter" %% "scala-uri" % "0.4.14"
libraryDependencies += "net.codingwell" %% "scala-guice" % "4.1.0"
libraryDependencies += "org.apache.hadoop" % "hadoop-client" % "2.6.0"
libraryDependencies += "org.ets.lca" % "ububi-lca-calculator" % "1.0"

libraryDependencies ++= Seq(
  ws
)

// The Play project itself
lazy val root = (project in file("."))
  .enablePlugins(Common, PlayScala, GatlingPlugin) //Common

// Documentation for this project:
//    sbt "project docs" "~ paradox"
//    open docs/target/paradox/site/index.html
lazy val docs = (project in file("docs")).enablePlugins(ParadoxPlugin).
  settings(
    //paradoxProperties += ("download_url" -> "https://example.lightbend.com/v1/download/play-rest-api")
  )

